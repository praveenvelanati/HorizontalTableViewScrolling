//
//  ViewController.swift
//  HorizontalTableView
//
//  Created by praveen velanati on 3/23/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit

class ViewController: UIViewController,UITableViewDelegate,UITableViewDataSource {

    @IBOutlet weak var tableView: UITableView!
    
    var characters = [Character]()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
   self.tableView.separatorStyle = UITableViewCellSeparatorStyle.SingleLine
        
   self.tableView.separatorColor = UIColor.cyanColor()
        
        var transform : CGAffineTransform = CGAffineTransformMakeRotation(-1.5707963)
        
        self.tableView.transform = transform
        
        self.tableView.pagingEnabled = true
        
        
        tableView.delegate = self
        tableView.dataSource = self
      parseCharacterCSV()
        
    }

    
    func parseCharacterCSV() {
        
        let path = NSBundle.mainBundle().pathForResource("pokemon", ofType: "csv")!
        
        do {
            
            let csv = try CSV(contentsOfURL : path)
            let rows = csv.rows
            
            for row in rows {
                
                let pokeId = Int(row["id"]!)!
                let name = row["identifier"]!
                let person = Character(name : name , characterId : pokeId)
               characters.append(person)
                
            }
            
            
        } catch let err as NSError {
            
            print(err.localizedDescription)
        }
        
        
        
        
    }

    
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if let cell = tableView.dequeueReusableCellWithIdentifier("cell") as? TableViewCell {
            
            let person = characters[indexPath.row]
            
            print(person.name)
            cell.configureCell(person)
            return cell
          
            
        }
            
            return UITableViewCell()
        
    }
    
    
    
    
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
    return characters.count
        
    }
    
    
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        
        return 1
    }

    
    func tableView(tableView: UITableView, willDisplayCell cell: UITableViewCell, forRowAtIndexPath indexPath: NSIndexPath) {
        
        
        
        var transform : CGAffineTransform = CGAffineTransformMakeRotation(1.5707963)
        
        cell.transform = transform
    }
    

}

