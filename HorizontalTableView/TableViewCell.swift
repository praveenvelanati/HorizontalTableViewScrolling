//
//  TableViewCell.swift
//  HorizontalTableView
//
//  Created by praveen velanati on 3/23/16.
//  Copyright © 2016 praveen velanati. All rights reserved.
//

import UIKit

class TableViewCell: UITableViewCell {

    @IBOutlet weak var imgView: UIImageView!
    
    @IBOutlet weak var nameLabel: UILabel!
    
    var character : Character!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        layer.cornerRadius = 2.0
       
    }

    override func setSelected(selected: Bool, animated: Bool) {

        super.setSelected(selected, animated: animated)

    }

    
    func configureCell(character : Character) {
        
        self.character = character
        
        nameLabel.text = character.name
        imgView.image = UIImage(named: "\(self.character.characterId)")
        
        
        
    }

    
    
}
